package com.custom.controlmoveview;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CustomMoveClockView mCustomMoveClockView;
    private TextView mTevShow;

    @SuppressLint("ObsoleteSdkInt")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mCustomMoveClockView = findViewById(R.id.customClockView);
        mTevShow = findViewById(R.id.tev_show);
        mCustomMoveClockView.setOnMoveListener(new CustomMoveClockView.OnMoveListener() {//接口回调
            @Override
            public void onMove(boolean isMove) {
                if (isMove) {
                    mTevShow.setText("移动中");
                } else {
                    mTevShow.setText("未移动");
                }
            }
        });
    }
}
